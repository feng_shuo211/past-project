package csc207.project.model;

import java.io.Serializable;
import java.util.Date;

public class Patient extends User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -966615119783154910L;
	
	// Represents the health card number
	private String healthCardNum;
	// Represents the urgency status of the patient
	private String status;
	// Represents the time the patient has been seen by a doctor
	private Date timeSeenByDoctor;
	// Represents the time the patient has arrived
	private Date timeArrival;

	/**
	 * Creates a patient with name, date of birth, health card number,
	 * urgency status, time that the patient has been seen by a doctor,
	 * tim the patient has arrived
	 * @param name - name of the patient
	 * @param dob - date of birth of the patient
	 * @param healthCardNum - health card number of the patient
	 * @param status - urgency status of the patient
	 * @param timeSeenByDoctor - time the patient has been seen by a doctor
	 * @param timeArrival - time the patient has arrived the hospital
	 */
	public Patient(String name, Date dob, String healthCardNum,
			String status, Date timeSeenByDoctor, Date timeArrival){
		super(name, dob);
		this.healthCardNum = new String(healthCardNum);
		this.status = new String(status);
		this.timeSeenByDoctor = (Date) timeSeenByDoctor.clone();
		this.timeArrival = (Date) timeArrival.clone();
	}

	/**
	 * Returns the health card number of the patient
	 * @return the healthCardNum of the patient
	 */
	public String getHealthCardNum() {
		return healthCardNum;
	}

	/**
	 * Sets the health card number to the patient
	 * @param healthCardNum - the number of the health card number
	 */
	public void setHealthCardNum(String healthCardNum) {
		this.healthCardNum = new String(healthCardNum);
	}

	/**
	 * Returns the urgency status of a patient
	 * @return status - the urgency status of a patient
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the urgency status of the patient
	 * @param status - urgency status of the patient
	 */
	public void setStatus(String status) {
		this.status = new String(status);
	}

	/**
	 * Returns the time the patient has been seen by a doctor
	 * @return timeSeenByDoctor - time the patient has been seen by a doctor
	 */
	public Date getTimeSeenByDoctor() {
		return timeSeenByDoctor;
	}

	/**
	 * Sets the time the patient has been seen by a doctor
	 * @param timeSeenByDoctor - time the patient has been seen by a doctor
	 */
	public void setTimeSeenByDoctor(Date timeSeenByDoctor) {
		this.timeSeenByDoctor = (Date) timeSeenByDoctor.clone();
	}

	/**
	 * Returns the time the patient has arrived
	 * @return timeArrival - time the patient has arrived
	 */
	public Date getTimeArrival() {
		return timeArrival;
	}

	/**
	 * Sets the time the patient has arrived
	 * @param timeArrival - time the patient has arrived
	 */
	public void setTimeArrival(Date timeArrival) {
		this.timeArrival = (Date) timeArrival.clone();
	}
}