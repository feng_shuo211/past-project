package csc207.project.model;

import java.util.Date;

public abstract class User {
	// Represents the name of the user
	private String name;
	// Represents the date of birth of the user
	private Date dob;

	/**
	 * Creates a user with name and date of birth
	 * @param name - name of the user
	 * @param dob - date of birth of the user
	 */
	public User(String name, Date dob){
		this.name = new String(name);
		this.dob = (Date) dob.clone();
	}

	/**
	 * Returns the name of the user
	 * @return the name of the user
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the user
	 * @param name to be set to the user
	 */
	public void setName(String name) {
		this.name = new String(name);
	}

	/**
	 * Returns the date of birth of the user
	 * @return the dob - date of birth of the user
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * Sets the date of birth of the user
	 * @param dob - date of birth to be set
	 */
	public void setDob(Date dob) {
		this.dob = (Date) dob.clone();
	}
}