package csc207.project.model;

import java.util.Date;

public class Symptoms {
	// Represents the description of the symptoms of the patient
	private String decription;
	// Represents the time the symptoms were collected
	private Date time;

	/**
	 * Creates a new symptoms description
	 * @param description - decription of the symptoms
	 * @param time - time the symptoms were collected
	 */
	public Symptoms(String decription, Date time){
		this.decription = new String(decription);
		this.time = (Date) time.clone();
	}

	/**
	 * Returns the symptoms description
	 * @return the description of the symptoms
	 */
	public String getDecription() {
		return decription;
	}

	/**
	 * Sets the new description of the symptoms
	 * @param decription of the symptoms
	 */
	public void setDecription(String decription) {
		this.decription = new String(decription);
	}

	/**
	 * Returns the time the symptoms were collected
	 * @return the time the symptoms were collected
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * Sets the time the symptoms were collected
	 * @param time the symptoms were collected
	 */
	public void setTime(Date time) {
		this.time = (Date) time.clone();
	}
}