package csc207.project.model;

import java.util.Date;

public class Nurse extends User {
	// Represents the login of the nurse
	private String login;
	// Represents the password of the nurse
	private String password;

	/**
	 * Constructs a Nurse with name, date of birth, login and password
	 * @param name - name of the nurse.
	 * @param dob - date of birth of the nurse.
	 * @param login - login of the nurse
	 * @param password - password of the nurse
	 */
	public Nurse(String name, Date dob, String login, String password){
		super(name,dob);
		this.login = new String(login);
		this.password = new String(password);
	}

	/**
	 * Returns the login of the nurse
	 * @return login - login of the nurse
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Sets the login of the nurse
	 * @param login - login of the nurse
	 */
	public void setLogin(String login) {
		this.login = new String(login);
	}

	/**
	 * Returns the password of the nurse
	 * @return password - password of the nurse
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password of the nurse
	 * @param password - password of the nurse
	 */
	public void setPassword(String password) {
		this.password = new String(password);
	}
}