package csc207.project.model;

import java.util.Date;

public class VitalSigns {
	// Represents the time the vital signs were collected
	private Date time;
	// Represents the temperature collected
	private float temperature;
	// Represents the systolic pressure collected
	private int systolic;
	// Represents the diastolic pressure collected
	private int diastolic;
	// Represents the heart rate collected
	private int heartRate;

	/**
	 * Creates a new entry of vital sign of a patient
	 * @param time - time that the vital signs were collected
	 * @param temperature - temperature collected
	 * @param systolic - systolic collected
	 * @param diastolic - diastolic collected
	 * @param heartRate - heart rate collected
	 */
	public VitalSigns(Date time, float temperature, int systolic,
			int diastolic, int heartRate){
		this.time = (Date) time.clone();
		this.temperature = temperature;
		this.systolic = systolic;
		this.diastolic = diastolic;
		this.heartRate = heartRate;
	}

	/**
	 * Returns the time the vital signs were collected
	 * @return the time the vital signs were collected
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * Sets the time the vital signs were collected
	 * @param time the vital signs were collected
	 */
	public void setTime(Date time) {
		this.time = (Date) time.clone();
	}

	/**
	 * Returns the temperature collected
	 * @return the temperature collected
	 */
	public float getTemperature() {
		return temperature;
	}

	/**
	 * Sets the temperature collected
	 * @param temperature collected
	 */
	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}

	/**
	 * Returns the systolic value collected
	 * @return the systolic value collected
	 */
	public int getSystolic() {
		return systolic;
	}

	/**
	 * Sets the systolic value collected
	 * @param systolic value collected
	 */
	public void setSystolic(int systolic) {
		this.systolic = systolic;
	}

	/**
	 * Returns the diastolic value collected
	 * @return the diastolic value collected
	 */
	public int getDiastolic() {
		return diastolic;
	}

	/**
	 * Sets the diastolic value collected
	 * @param diastolic value collected
	 */
	public void setDiastolic(int diastolic) {
		this.diastolic = diastolic;
	}

	/**
	 * Returns the heart rate collected
	 * @return the heartRate collected
	 */
	public int getHeartRate() {
		return heartRate;
	}

	/**
	 * Sets the heart rate value collected
	 * @param heartRate value collected
	 */
	public void setHeartRate(int heartRate) {
		this.heartRate = heartRate;
	}
}