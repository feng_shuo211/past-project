package csc207.project.nursehelper;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class PatientInfo extends Activity {

	private Button addVitalSigns;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_info);
		
		addVitalSigns = (Button) findViewById(R.id.add_vital_signs);
		addVitalSigns.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intentAddVitalSigns = new Intent(getBaseContext(), AddVitalSigns.class);
				startActivity(intentAddVitalSigns);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_info, menu);
		return true;
	}

}
