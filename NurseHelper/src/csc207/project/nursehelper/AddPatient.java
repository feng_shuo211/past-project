package csc207.project.nursehelper;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import csc207.project.model.Patient;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class AddPatient extends Activity {
	private UserManager manager;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_patient);
		try {
            manager = new UserManager();
    } catch (IOException e) {
            e.printStackTrace();
    }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_patient, menu);
		return true;
	}
	
	@SuppressLint("SimpleDateFormat")
	public void addPatient(View view){
		Intent intent = new Intent(this, AddVitalSigns.class);
		EditText nameText = (EditText) findViewById(R.id.name_field);
		String name = nameText.getText().toString();
		EditText dobText = (EditText) findViewById(R.id.dob_field);
		String dobString = dobText.getText().toString();
		EditText hcnText = (EditText) findViewById(R.id.hcn_field);
		String hcn = hcnText.getText().toString();
		Date date = new Date(); 
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");  
		Date dob = null;
		try {
			dob = format1.parse(dobString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Patient patient = new Patient(name, dob, hcn, new String(""), new Date(), date);
		manager.addPatient(patient);
		intent.putExtra("patientKey", patient);
		startActivity(intent);
	}
		
	public void back(View view){
		Intent intent = new Intent(this, MainMenu.class);
		startActivity(intent);
	}

}
