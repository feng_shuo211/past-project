package csc207.project.nursehelper;


import csc207.project.model.Nurse;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
public class Login extends Activity{
	
	private UserManager userManager;
	
	private Button login_button;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
//		login_button = (Button) findViewById(R.id.login_button);
//		
//		login_button.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				login(v);
//			}
//		});
		
	}
	
	
	public void login(View view){
		
		//Then intent for going to next screen.
		Intent intent = new Intent(this,MainMenu.class);
		//Gets the user_name from the login_username text field.
		EditText loginuserNameText = (EditText) findViewById(R.id.login_username);
		String login_username = loginuserNameText.getText().toString();
		
		//Gets the password from the login_password text field.
		EditText loginpasswordText = (EditText) findViewById(R.id.login_password);
		String login_password = loginpasswordText.getText().toString();
		
		//Check the user_name and password are in the file.
		if (userManager.getNurses().containsKey(login_username)){
			if (userManager.getNurses().get(login_username).equals(login_password)){
				// The Nurse can go to the main page.
				startActivity(intent);
			}
			else {
				//The password is wrong
				AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
				
				dlgAlert.setMessage("wrong password");
	            dlgAlert.setTitle("Error Message...");
	            dlgAlert.setPositiveButton("OK", null);
	            dlgAlert.setCancelable(true);
	            dlgAlert.create().show();

	            dlgAlert.setPositiveButton("Ok",
	                    new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int which) {

	                }
	            });
			}
		}
		else {
			//There doens't have this Nurse
			AlertDialog.Builder dlgAlert1 = new AlertDialog.Builder(this);
			
			dlgAlert1.setMessage("Wrong user name");
            dlgAlert1.setTitle("Error Message...");
            dlgAlert1.setPositiveButton("OK", null);
            dlgAlert1.setCancelable(true);
            dlgAlert1.create().show();

            dlgAlert1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });
		}
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

}
