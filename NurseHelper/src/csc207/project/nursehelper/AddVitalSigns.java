package csc207.project.nursehelper;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import csc207.project.model.Patient;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class AddVitalSigns extends Activity {
	private Patient patient;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_vital_signs);
		
		Intent intent = getIntent();
		
		patient = (Patient) intent.
				getSerializableExtra("patientKey");
		String patientName = patient.getName();
		TextView patientToAddVital = (TextView) findViewById
				(R.id.patient_to_add_vital);
		patientToAddVital.setText("Add vital signs for " + patientName);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action 
		// bar if it is present.
		getMenuInflater().inflate(R.menu.add_vital_signs, menu);
		return true;
	}
	public void addVitalSigns(View view){
		Intent intent = new Intent(this, AddVitalSigns.class);
		
		startActivity(intent);
	}
		
	public void back(View view){
		Intent intent = new Intent(this, MainMenu.class);
		startActivity(intent);
	}

}
