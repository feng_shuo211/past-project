package csc207.project.nursehelper;

import java.util.ArrayList;

import csc207.project.model.Patient;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class MainMenu extends Activity {

	private UserManager userManager;
	
	private Button addPatient;
	private Button saveAll;
	private Button logout;
	
	private EditText search;
	
	private ArrayList<String> list;
	private ArrayList<String> listToShow;
	private ArrayAdapter<String> adapter;
	private ListView listView;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		
		addListener();
		
		listView = (ListView) findViewById(R.id.listPatient);
		Intent intent = getIntent();
		userManager = (UserManager) intent.getSerializableExtra("userManager");
		if(userManager != null) {
			for(String healthCardNum : userManager.getPatients().keySet()) {
				list.add(healthCardNum);
			}
			
			listToShow.addAll(list);
			
			adapter = new ArrayAdapter<String>(this, R.layout.activity_main_menu, listToShow);
			listView.setAdapter(adapter);
			
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, final View view,
						int position, long id) {
					final String item = (String) parent.getItemAtPosition(position);
					
			    }
			});	
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}
	
	public void addListener() {
		addPatient = (Button) findViewById(R.id.add_patient);
		saveAll = (Button) findViewById(R.id.save_all);
		logout = (Button) findViewById(R.id.logout);
		
		search = (EditText) findViewById(R.id.search);
	
		addPatient.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intentAddPatient = new Intent(getBaseContext(), AddPatient.class);
				startActivity(intentAddPatient);
			}
		});
		
		saveAll.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(userManager != null) {
					userManager.saveToFile();
				}
			}
		});
		
		logout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		search.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				listToShow.addAll(list);
				for(String entry : listToShow) {
					if(entry.contains(s)) {
						listToShow.remove(entry);
					}
				}
			}
		});
	}
}
