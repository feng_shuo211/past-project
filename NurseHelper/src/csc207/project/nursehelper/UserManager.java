package csc207.project.nursehelper;

import csc207.project.model.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class UserManager implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private final static String NURSE_FILENAME = "nurses.db";
	/**
	 * 
	 */
	private final static String PATIENT_FILENAME = "patients.db";
	/**
	 * 
	 */
	private Map<String, Patient> patients;
	/**
	 * 
	 */
	private Map<String, Nurse> nurses;
	
	/**
	 * 
	 * @throws IOException
	 */
	public UserManager() throws IOException {
		patients = new HashMap<String, Patient>();
		nurses = new HashMap<String, Nurse>();
		
        File nurseFile = new File(NURSE_FILENAME);
        if (nurseFile.exists()) {
            readNurses(nurseFile.getPath());
        } else {
            nurseFile.createNewFile();
        }
        
        File patientFile = new File(PATIENT_FILENAME);
        if (patientFile.exists()) {
            readFromFile();
        } else {
            patientFile.createNewFile();
        }
	}
	
	/**
	 * 
	 * @param filePath
	 * @throws FileNotFoundException
	 */
	public void readNurses(String filePath) throws FileNotFoundException {
		Scanner scanner = new Scanner(new FileInputStream(filePath));
		String[] record;
		Nurse nurse;
		
		while(scanner.hasNextLine()) {
			record = scanner.nextLine().split(",");
			nurse = new Nurse(record[0], Date.valueOf(record[1]), record[2], 
					record[3]);
			nurses.put(record[2], nurse);
		}
	}
	
	/**
	 * 
	 * @throws FileNotFoundException
	 */
	public void readFromFile() throws FileNotFoundException {
		Scanner scanner = new Scanner(new FileInputStream(PATIENT_FILENAME));
		String[] record;
		Patient patient;
		
		while(scanner.hasNextLine()) {
			record = scanner.nextLine().split(",");
			patient = new Patient(record[0], Date.valueOf(record[1]), 
					record[2], record[3], Date.valueOf(record[4]), 
					Date.valueOf(record[5]));
			patients.put(record[2], patient);
		}
	}
	
	/**
	 * 
	 * @param outputStream
	 */
	@SuppressWarnings("resource")
	public void saveToFile() {
		try {
			File file = new File(PATIENT_FILENAME);
 
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for (Patient patient : patients.values()) {
                bw.write((patient.toString() + "\n"));
            }
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the patients
	 */
	public Map<String, Patient> getPatients() {
		return patients;
	}

	/**
	 * @return the nurses
	 */
	public Map<String, Nurse> getNurses() {
		return nurses;
	}
	
	public void addPatient(Patient patient){
		patients.put(patient.getHealthCardNum(), patient);
	}
}
