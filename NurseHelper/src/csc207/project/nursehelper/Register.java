package csc207.project.nursehelper;


import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Calendar;

import csc207.project.model.Nurse;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
public class Register extends Activity implements OnItemSelectedListener{
	/**The name of the file used to store the information of the nurse.*/
	public static final String FILENAME = "nurse_info.txt";
	
	/**The UserManager used to managed Nurse for this application*/
	private UserManager manager;
	
	/**The birthday of the Nurse.*/
	private Calendar birthday;
	/**A spinner of day of birthday for Nurse to select.*/
	private Spinner spinner_day;
	
	/**A spinner of month of birthday for Nurse to select. */
	private Spinner spinner_month;
	
	/**A spinner of year of birthday for Nurse to select. */
	private Spinner spinner_year;
	
	/**A list of days for User to select.*/
	private String[] days={"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"
			,"16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
	
	/**A list of months for User to select. */
	private String[] month = {"January","February","March","April","May","June","July","August","9","10","11","12"};
	
	/**A list of years for User to select.*/
	Integer year = 1999;
	private List<String> years = new ArrayList<String>();
	
	/** A Button for register.*/
	private Button register;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		birthday = new GregorianCalendar();
		
		//Create Adapter for Spinner_day.
		ArrayAdapter<String> adapter_day = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,days);
		spinner_day =(Spinner) findViewById(R.id.spinner_day);
		spinner_day.setAdapter(adapter_day);
		
		spinner_day.setOnItemSelectedListener(this);
		//Create Adapter for Spinner_month
		ArrayAdapter<String> adapter_month = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, month);
		spinner_month = (Spinner) findViewById(R.id.spinner_month);
		spinner_month.setAdapter(adapter_month);
		
		//cREATE Adapter for Spinner_year
		//A for loop for creating the ArrayAdapter.
		for (int i=0;i<50;i++){
			years.add(year.toString());
			year = year-1;
		}
		ArrayAdapter<String> adapter_year = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,years);
		spinner_year = (Spinner) findViewById(R.id.spinner_year);
		spinner_year.setAdapter(adapter_year);
		
		//Makes the button works.
		register = (Button) findViewById(R.id.button_register);
		
		register.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				register(v);
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}
	
	public void register(View view){
		
		//Gets the first name from first_name the text field.
		EditText usernametext = (EditText) findViewById(R.id.user_name);
		String username = usernametext.getText().toString();
		
		//Gets the password from the password text field.
		EditText passwordtext = (EditText) findViewById(R.id.pass_word);
		String password = passwordtext.getText().toString();

		
		//Creates the initial of the Nurse
		String initial = username.substring(0,2);
	
		//Creates a Nurse
		Date user_birthday = birthday.getTime();
		Nurse nurse = new Nurse(username,user_birthday,initial,password);
		
		//Adds this Nurse to the manager.
		//UserManager.add(Nurse);
		
		//Write the infomation about the nurse to file.
		File f = new File("nurses.tet");
		try{
			FileWriter fw = new FileWriter(f);
			fw.write(username+","+password+","+initial+","+user_birthday);
		}catch(Exception ex){
			
		}
	}


	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
		//The day of day of the birthday
		int day = Integer.parseInt(spinner_day.getSelectedItem().toString());
		
		//The month of month of the birthday
		String month = spinner_month.getSelectedItem().toString();
		
		//Switch month to a integer.
		int months = 0;
        if (month.equals("January")){
        	months = 1;
        }
        
        else if (month.equals("Feburay")){
        	months = 2;
        }
        else if (month.equals("March")){
        	months = 3;
        }
        else if (month.equals("April")){
        	months = 4;
        }
        else if (month.equals("May")){
        	months = 5;
        }
        else if (month.equals("June")){
        	months = 6;
        }
        else if (month.equals("July")){
        	months = 7;
        }
        else if (month.equals("August")){
        	months = 8;
        }
        else if (month.equals("September")){
        	months = 9;
        }
        else if (month.equals("Octorber")){
        	months = 10;
        }
        else if (month.equals("November")){
        	months = 11;
        }
        else if (month.equals("December")){
        	months = 12;
        }
        
        //The year of year of the birthday.
        int year = Integer.parseInt(spinner_year.getSelectedItem().toString());
        
        birthday.set(Calendar.DATE, day);
        birthday.set(Calendar.MONTH, months);
        birthday.set(Calendar.YEAR, year);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
}
