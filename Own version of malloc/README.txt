1. Data Structure
	The data structure I use is a linked list. I use this linked list to track
all the allocated memory address. Each element contains a size which is the size
of this address, a pointer which is the pointer that points to this address, a 
variable "free" that represents this memory is currently using or not, and a pointer 
that points to next element.

2. Algorithm
	mymalloc:
	As I have linked list, I can find all freed and non-freed allocated memory by going
through this linked list. At each mymalloc call, my algorithm will go using 
the "First Fit Algorithm" to through this linked list, if there is an element that 
has enough size for the size we need,then the algorithm will pick this element and 
if the size is more than we need, it will split the element into two elements,then 
we have a track of the rest memory. If there isn't any element that has enough size, 
then the algorithm will extend more memory.
	myfree:
	Myfree function will just simply go through the linked list, to check each element's 
address pointer, if the pointer matched, then set the free mode of this element to 1, which 
means this memory is free. If we can find any pointer that match the input pointer, then
the algorithm will just print "We didn't allocate this memory".