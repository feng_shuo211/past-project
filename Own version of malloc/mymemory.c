#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#define SYSTEM_MALLOC 1
#define LINKED_LIST_SIZE 40
#define SIZE 1024


/*need a linked list for storing the pointer of each allocated memory.*/
typedef struct linked_list{
	unsigned int size;
	struct linked_list *next;
	int free;

	void *current;
	//int num;
	struct linked_list *prev;
}linked_list;

struct linked_list *free_head=NULL; //THe linked list which is using to store currently freed memory space.

//counter for checking if its the first call
int counter = 0;

//for the size of each block
unsigned int s_temp = 0;

//the lock that for multithread.
pthread_mutex_t lock;

//the pointer that for current block
void *reach_pointer = NULL;

//the size for how many bytes we can use.
int size_counter =0;

//the function that for setting up new block
void setup_free(struct linked_list **ptr, unsigned int size);

//the function that for spliting
struct linked_list *spliting(struct linked_list **ptr, unsigned int size);

/* mymalloc: allocates memory on the heap of the requested size. The block
             of memory returned should always be padded so that it begins
             and ends on a word boundary.
     unsigned int size: the number of bytes to allocate.
     retval: a pointer to the block of memory allocated or NULL if the 
             memory could not be allocated. 
             (NOTE: the system also sets errno, but we are not the system, 
                    so you are not required to do so.)
*/
void *mymalloc(unsigned int size) {
	pthread_mutex_lock(&lock);	
	
	if (counter == 0){
		s_temp=sizeof(free_head);
		setup_free(&free_head,size);
		counter = 1;

		pthread_mutex_unlock(&lock);
		return free_head->current;
	}
	else{
		//need to check the freed list;
		struct linked_list *temp =NULL;
		temp = free_head;
		
		struct linked_list *p =NULL;
		if (temp == NULL){
			perror("temp == NULL\n");
		}
	
		int added = 0;
	
		//we go through the linked list, if we find a space is large enough, then we will use it
		while(temp != NULL && added == 0){	
			
			//We get enought size
			if ((temp->size >= size) && (temp->free == 1)){
				//exactly the same
				if(temp->size == size){
					added = 1;	
					temp->free = 0;

					pthread_mutex_unlock(&lock);
					return temp->current;
				}
				//need to split.
				else if (temp->size > size+24+48){
					
					added = 1;
					
					struct linked_list *sp;
				
				
					sp =spliting(&temp,size);
				
					sp->next = temp->next;
				
					temp->next=sp;
				
					temp->size =size;
					temp->free =0;
				
					pthread_mutex_unlock(&lock);
					return temp->current;
				}
				
				
				
			}
			p = temp;
			temp = temp->next;
			
			
		}	
	
		/*If we reach here, then we need to use sbrk to make more space */
		/*search used list, to find an element to store it */
		setup_free(&temp,size);
		p->next = temp;
		counter = counter + 1;
		added=1;
		
		if (temp == NULL){
			perror("temp is still null\n");

		}
		if(temp){
			
			pthread_mutex_unlock(&lock);
			return temp->current;
		}
	
	}
	return (void *)1;
}

/* myfree: unallocates memory that has been allocated with mymalloc.
     void *ptr: pointer to the first byte of a block of memory allocated by 
                mymalloc.
     retval: 0 if the memory was successfully freed and 1 otherwise.
             (NOTE: the system version of free returns no error.)
*/
unsigned int myfree(void *ptr) {
	
	pthread_mutex_lock(&lock);
	int c = 0;
	struct linked_list *temp=NULL;
	temp = free_head;
	if(temp == NULL){
		perror("Temp is null\n");
		return 1;

	}
	//going through all element
	while(temp){
		
		if (temp->current == ptr){
			
			temp->free = 1;
			
			pthread_mutex_unlock(&lock);
			
			return 0;
		}
		c++;
		temp= temp->next;

	}


	pthread_mutex_unlock(&lock);

	
	return 1;
}

/*The function that for seting up blocks*/
void setup_free(struct linked_list **ptr, unsigned int size){
	

	//the new block
	struct linked_list *new=NULL;
	unsigned int s = LINKED_LIST_SIZE;
		
	unsigned int t;
	t = LINKED_LIST_SIZE+size;
	unsigned int size_temp = s + size;
	
	if(counter == 0){
		new = (void *)sbrk(0);

	}else{
		new = (void *)reach_pointer;

	}
	
	//sbrk more size.
	while(size_counter < t){
		sbrk(SIZE);
		size_counter = size_counter + 1024;
		
	}
	size_counter = size_counter - size_temp;
	
	//pointer for current block
	reach_pointer = (void *)new+size_temp;
	
	new->current = (void *)new+LINKED_LIST_SIZE;

	
	new->size = size;
	new->next = NULL;
	
	new->free = 0;	
	if (*ptr != NULL){
		
		(*ptr)->next = new;
		
	}
	else{
		
		*ptr = new;	
	}

}

/*The function that for spliting*/
struct linked_list *spliting(struct linked_list **ptr,unsigned int size){

	struct linked_list *new;
	
	new = (*ptr)->current+size;

	new->current = (void *)new+LINKED_LIST_SIZE;
	new->size = (*ptr)->size - size-LINKED_LIST_SIZE;

	new->free = 1;

	return new;

}

