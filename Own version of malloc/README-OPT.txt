The change I have made compare to phase1:

	-Added a splitting function, that for split blocks.
		For example: We have a free block and the size of it is 100, if we want to allocate 50 bytes of this address, then we split the blocks into two. One is for the first 50, another one is for the next 50 bytes, then we don't waste any of them.
	
	-Added a coalescing method.
		This method will make two or three adjacent free block into one, which means we can get a larger blocks from smaller blocks.This will increase the efficiency.This function we make the unused block disappear and make the memory that the unused block used into free.
