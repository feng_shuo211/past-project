#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#define SYSTEM_MALLOC 1
#define LINKED_LIST_SIZE 48
#define SIZE 1024

/*the linked list structure*/
typedef struct linked_list{
	unsigned int size;
	struct linked_list *next;
	int free;
	//pthread_mutex_t lock;
	void *current;
	int head;
	int tail;
	struct linked_list *prev;
}linked_list;

struct linked_list *free_head=NULL; //THe linked list which is using to keep track of the memory

//For check if it is the first time call mymalloc
int counter = 0;

//the size of the free_head
unsigned int s_temp = 0;

//the lock that for multithread enviroment
pthread_mutex_t lock;

//the pointer that for current break
void *reach_pointer = NULL;

//the number of how many size we can use
int size_counter =0;

//the function that for seting up new block
void setup_free(struct linked_list **ptr, unsigned int size);

//the function that for spliting blocks.
struct linked_list *spliting(struct linked_list **ptr, unsigned int size);

//the function that for coalesing blocks
void coal(struct linked_list **ptr);

/* mymalloc: allocates memory on the heap of the requested size. The block
             of memory returned should always be padded so that it begins
             and ends on a word boundary.
     unsigned int size: the number of bytes to allocate.
     retval: a pointer to the block of memory allocated or NULL if the 
             memory could not be allocated. 
             (NOTE: the system also sets errno, but we are not the system, 
                    so you are not required to do so.)
*/


/*need a linked list for storing the pointer of each allocated memory.*/

void *mymalloc(unsigned int size) {
	pthread_mutex_lock(&lock);	
	//initialize free head
	if (counter == 0){
		/*If it is the first time calling.*/
		s_temp=sizeof(free_head);
		setup_free(&free_head,size);
		free_head->head = 1;
		counter = 1;
		free_head->tail = 0;
		//printing(free_head);	
		pthread_mutex_unlock(&lock);
		return free_head->current;
	}
	else{
		/*If is not the first time calling*/
		struct linked_list *temp =NULL;
		temp = free_head;
		struct linked_list *p =NULL;
		if (temp == NULL){
			printf("temp == NULL, in counter :%d\n",counter);
			exit(1);
		}
	
		int added = 0;
	
		//we go through the linked list, if we find a space is large enough, then we will use it
		while(temp != NULL && added == 0){
			if ((temp->size >= size) && (temp->free == 1)){
				if(temp->size == size){
					//we find a space that is exactly same size as we need.
					added = 1;	
					temp->free = 0;
					pthread_mutex_unlock(&lock);
					return temp->current;
				}
						
				else if (temp->size > size+LINKED_LIST_SIZE){
					//we find a space that large enough, then we need to split it into two.

					added= 1;
			
					struct linked_list *sp;
					sp = spliting(&temp,size);
					
					
					sp->next = temp->next;
					sp->prev = temp;
					if(temp->tail == 1){
						sp->tail = 1;
						temp->tail = 0;
					}else{
						sp->tail =0;

					}
					temp->next = sp;
					temp->size = size;
					temp->free = 0;
					pthread_mutex_unlock(&lock);
					return temp->current;
				}
				
				
			}
			p = temp;
			temp = temp->next;
			
			
		}	
	
		/*If we reach here, then we need to use sbrk to make more space */
		/*search used list, to find an element to store it */
		setup_free(&temp,size);
		p->next = temp;
		temp->prev = p;
		p->tail = 0;
		temp->tail = 1;
		counter = counter + 1;
		added=1;
		
		if (temp == NULL){
			perror("temp is still null\n");
			exit(1);

		}
		if(temp){
			
			pthread_mutex_unlock(&lock);
			return temp->current;
		}
	
	}
	return (void *)1;
}

/* myfree: unallocates memory that has been allocated with mymalloc.
     void *ptr: pointer to the first byte of a block of memory allocated by 
                mymalloc.
     retval: 0 if the memory was successfully freed and 1 otherwise.
             (NOTE: the system version of free returns no error.)
*/
unsigned int myfree(void *ptr) {
	
	pthread_mutex_lock(&lock);
	int c = 0;
	struct linked_list *temp=NULL;
	temp = free_head;
	
	if(temp == NULL){
		perror("Temp is null\n");
		pthread_mutex_unlock(&lock);
		return 1;

	}
	if (ptr == (void *)NULL){
		perror("Freeing a null pointer\n");
		pthread_mutex_unlock(&lock);
		return 1;


	}
	
	while(temp){
		
		if (temp->current == ptr){
			
			temp->free = 1;
			
			//try coalscing with next block
			coal(&temp);
			//try to coalscing with the previous block.
			if(temp->prev){
				coal(&temp->prev);

			}
			pthread_mutex_unlock(&lock);
			
			return 0;
		}
		c++;
		temp= temp->next;

	}

	perror("We didn't allocate this memory\n");
	pthread_mutex_unlock(&lock);

	return 1;
}

/* A function for adding new blocks*/
void setup_free(struct linked_list **ptr, unsigned int size){
	//A new block
	struct linked_list *new=NULL;
	unsigned int s = LINKED_LIST_SIZE;
		
	//The size we need for block and size
	unsigned int t;
	t = LINKED_LIST_SIZE+size;

	unsigned int size_temp = s + size;
	
	//check to if is the first block, otherwise set the block's address to the current break
	if(counter == 0){
		new = (void *)sbrk(0);

	}else{
		new = (void *)reach_pointer;

	}

	//using sbrk() to get enough size
	while(size_counter < t){
		sbrk(SIZE);
		size_counter = size_counter + 1024;


	}
	
	size_counter = size_counter - size_temp;
	
	//The pointer that for current break
	reach_pointer = (void *)new+t;
	
	new->current = (void *)new+LINKED_LIST_SIZE;
	new->size = size;
	new->next = NULL;
	new->head = 0;
	//new->prev = *ptr;
	new->free = 0;	

	if (*ptr != NULL){		
		new->prev=(*ptr);
		(*ptr)->next = new;
		
	}
	else{
		*ptr = new;	
	}

}

/*A function that for spliting blocks*/

struct linked_list *spliting(struct linked_list **ptr,unsigned int size){
	
	struct linked_list *new;
	
	new = (*ptr)->current+size;
	new->current = (void *)new+LINKED_LIST_SIZE;
	new->size = (*ptr)->size -size-LINKED_LIST_SIZE;
	new->free = 1;

	return new;


}

/*A function that for coalescing*/
			
void coal(struct linked_list **ptr){

	/*check the next block*/
	if((*ptr)->next && (*ptr)->next->free==1){
		
		unsigned int s = (*ptr)->size + (*ptr)->next->size + LINKED_LIST_SIZE;
		(*ptr)->size = s;
		(*ptr)->next = (*ptr)->next->next;
		if((*ptr)->next){
			(*ptr)->next->prev = (*ptr);

		}

	}

}

